const request = require('request')
const semver = require('semver')    // 版本和版本范围的解析、计算、比较
const chalk = require('chalk')      // consoel颜色处理的插件
const packageConfig = require('../package.json')

module.exports = done => {
  // 检查支持的最低node版本
  if (!semver.satisfies(process.version, packageConfig.engines.node)) {
    return console.log(chalk.red(
      '  You must upgrade node to >=' + packageConfig.engines.node + '.x to use vue-cli'
    ))
  }
  // npmjs库里面的版本号
  request({
    url: 'https://registry.npmjs.org/vincent-cli',
    timeout: 1000
  }, (err, res, body) => {
    if (!err && res.statusCode === 200) {
      const latestVersion = JSON.parse(body)['dist-tags'].latest
      const localVersion = packageConfig.version
      if (semver.lt(localVersion, latestVersion)) {
        console.log(chalk.yellow('  A newer version of vincent-cli is available.'))
        console.log()
        console.log('  latest:    ' + chalk.green(latestVersion))
        console.log('  installed: ' + chalk.red(localVersion))
        console.log()
      }
    }
    done()
  })
}
